Rails.application.routes.draw do
  
  resources :comments
  
  get 'posts/feed', to: 'posts#feed', as: :feed
  delete 'posts/destroy/:id', to: 'posts#destroy', as: :post_destroy
  resources :posts
  
  post 'posts/:id/comments', to: 'comments#create', as: :comment_post
  get 'posts/comment/:id', to: 'comments#edit', as: :edit_comment_post
  patch 'posts/:id/like', to: 'posts#like', as: :like_post
  patch 'posts/:id/dislike', to: 'posts#dislike', as: :dislike_post
  
  
  # Rotas de UserController
  get 'users/admin', to: 'users#crud', as: :admin
  get 'users', to: 'users#index', as: :users
  get 'users/new', to: 'users#new', as: :user_new
  post 'users/new', to: 'users#create'
  get 'users/:id', to: 'users#show', as: :user
  get 'users/edit/:id', to: 'users#edit', as: :user_edit
  patch 'users/edit/:id', to: 'users#update'
  delete 'users/destroy/:id', to: 'users#destroy', as: :user_destroy
  
  #Rotas de SessionsController
  get 'login', to: 'sessions#new', as: :login
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy', as: :logout
  

  
end
