class User < ApplicationRecord
  
  has_secure_password
  
  validates :name, length: { in: 2..50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, length: { in: 10..50}, format: { with: VALID_EMAIL_REGEX }
  #allow_nil permite que voce nao precise redigitar a senha quando editar o usuario
  validates :password, length: { in: 3..6, presence: true, allow_nil: true }
  
  
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :like_posts ,dependent: :destroy
  has_many :dislike_posts ,dependent: :destroy
  
end
