class Post < ApplicationRecord
  mount_uploader :image, PictureUploader
  
  validates :title, length: {in: 2..50}
  validate  :picture_size
  validate  :post_content
  
  
  belongs_to :user
  
  has_many :comments, dependent: :destroy
  has_many :like_posts ,dependent: :destroy
  has_many :dislike_posts ,dependent: :destroy

  def picture_size
    if image.size > 2.megabytes
      errors.add(:image, "imagem não pode ser maior a 2 megabytes")
    end
  end
  
  def post_content
    if image.length > 0
              if content.length > 0
                errors.add(content, "nao pode postar 2 coisas   linha 23")
              end
    else
      
      if content.length < 2 || content.length > 140
        errors.add(content, "conteudo fora dos limites  linha 28")
      end
    end
  end
  
  
end
