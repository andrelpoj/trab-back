class Comment < ApplicationRecord
  belongs_to :user,  optional: true
  belongs_to :post
  
  validates :content, length: {in:5..50}
  
end
