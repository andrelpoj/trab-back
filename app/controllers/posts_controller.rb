class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy, :like, :dislike]
  before_action :non_logged_user, except: [:show,:feed]
  before_action :correct_user, only: [:edit,:update]
  before_action :admin_or_correct_user, only: :destroy
  before_action :non_admin, only: [:index]
  #after_action :destroy, only: [:dislike], if: -> { @odiado }
  
  #def teste
  #  redirect_to post_destroy_path(@post.id)
  #end
  
  
  def feed
        @posts = Post.paginate(:page => params[:page], :per_page => 5)
  end
  
  # GET /posts
  # GET /posts.json
  def index
    # paginate
    @posts = Post.paginate(:page => params[:page], :per_page => 5)
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    
    @comment = Comment.new
    @comments = @post.comments
    
    if logged_in?
      @liked = LikePost.find_by(user_id: current_user.id, post_id: @post.id)
      @disliked = DislikePost.find_by(user_id: current_user.id, post_id: @post.id)
    end
    
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    
    @post = current_user.posts.build(post_params)
    # if !(@post.content.empty? && (@post.image.size>0)?)
      
      respond_to do |format|
          if @post.save
            format.html { redirect_to @post, notice: 'Post was successfully created.' }
            format.json { render :show, status: :created, location: @post }
          else
            format.html { render :new }
            format.json { render json: @post.errors, status: :unprocessable_entity }
          end
      end  
    
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to feed_path, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
    
  end



  def like
    like = LikePost.find_by(post_id: @post.id, user_id: current_user.id)
    
    if like.nil?
      LikePost.create(post_id: @post.id, user_id: current_user.id)
    else
      like.destroy
    end
    
    dislike = DislikePost.find_by(post_id: @post.id, user_id: current_user.id)
    dislike.destroy if dislike
    
    @liked = LikePost.find_by(user_id: current_user.id, post_id: @post.id)
    
    respond_to do |format|
      format.html { redirect_to @post }
      format.js
    end
    
  end


  def dislike

    dislike = DislikePost.find_by(post_id: @post.id, user_id: current_user.id)
    if dislike.nil?
      DislikePost.create(post_id: @post.id, user_id: current_user.id)
    else
      dislike.destroy
    end
    like = LikePost.find_by(post_id: @post.id, user_id: current_user.id)
    like.destroy if like
    
    @disliked = DislikePost.find_by(user_id: current_user.id, post_id: @post.id)
    respond_to do |format|
      format.html { redirect_to @post }
      format.js
    end
    
    #@odiado = false
    if  DislikePost.count(:id == @post.id) > 14 # o numero vai ser o valor limite
      #@odiado = true
      # redirect_to post_destroy_path(@post.id)
      @post.destroy
      flash[:error] = "Esse post foi deletado por completar 15 dislikes."
      respond_to do |format|
        format.html {redirect_to feed_path}
        format.js {render plain: "window.location = '/posts/feed';"}
      end
    end
  end


  private
  
  
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:user_id, :title, :content, :image)
    end
        
    
    def correct_user
      if @post.user != current_user
        redirect_to posts_path
      end
    end
    
    def admin_or_correct_user
      if !current_user.admin && current_user != @post.user
        redirect_to posts_path
      end
    end
    
    def non_admin
      if !current_user
        redirect_to login_path
      end
      if (current_user && !current_user.admin)
        redirect_to user_path(current_user.id)
      end
    end
    
end
