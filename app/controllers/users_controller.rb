class UsersController < ApplicationController
    before_action :set_user, only: [:show, :edit, :destroy, :update]
    #before_action :user_params, only: [:create, :update]
    
    before_action :logged_user, only: [:new, :create]
    before_action :non_logged_user, except: [:new, :create]
    before_action :non_admin, only: :index
    before_action :correct_user_or_admin,only: [:edit,:update,:destroy]
    
    def crud
        @users = User.paginate(:page => params[:page], :per_page => 5)
    end
    
    def new 
        @user = User.new
    end
    
    def create
        @user = User.new(user_params)
        if @user.save
           redirect_to user_path(@user)
        else
            render :new
        end
    end
    
    def show
    end
    
    def edit
    end
    
    def update
        if @user.update_attributes(user_params)
            redirect_to user_path(@user)
        else
            render :edit
        end
    end
    
    def index
        #Variaveis com @ sao variaveis de instancia
        #elas ficam disponiveis para as views
        @users = User.all
    end
    
    def destroy
        if @user == current_user
            log_out
        end
        
        @user.destroy
        
        redirect_to users_path
    end
    
    private
    
    def set_user
       @user = User.find(params[:id]) 
    end
    
    def user_params
        @user_params = params.require(:user).permit(:name, :email, :password, :password_confirmation, :feeder, :profile_photo)
    end
    
    def correct_user_or_admin
       if !(current_user == @user || current_user.admin)
           redirect_to user_path(current_user)
       end
    end
    
    def non_admin
      if !current_user
        redirect_to login_path
      end
      if (current_user && !current_user.admin)
        redirect_to user_path(current_user.id)
      end
    end
end
